import h5py
import numpy as np
import PIL.Image
import os
from scipy.optimize import curve_fit

def ReadImage(filename):
    #Load data file
    file =  h5py.File(filename,'r')
    assert('data' in file.keys() and 'mapping' in file['data'] and 'sensor' in file['data']), 'incorrect fileformat'

    mapping = file['data']['mapping']
    sensor = file['data']['sensor']

    # extract image dimensions
    assert ('image_width' in file['data'].attrs and 'image_height' in file['data'].attrs), 'missing metadata'
    width = file['data'].attrs['image_width']
    height = file['data'].attrs['image_height']

    #Check if correct dimensions
    assert(width*height == np.sum(mapping[:] == 2)), 'Incorrect dimensions'

    #Extract all pixels values from sensor data
    pixelData = ExtractData(sensor,mapping)

    #Reshape pixeldata to correct image size
    imArray = np.reshape(pixelData, (height, width))

    #Return 8 bit or 16 bit depending on max val
    return imArray.astype(np.uint8) if np.max(imArray)<256 else imArray.astype(np.uint16)

def ExtractData(sensor,mapping):
    sensor = np.array(sensor)

    #Set unused data to 0
    sensor[mapping[:]==0] = 0

    #sum all pixels values, extract summed value at last sample of pixel, then subtract previous
    sensor = np.cumsum(sensor)
    sensor = sensor[mapping[:]==2]
    sensor[1:] -= sensor[:-1]
    return sensor

def Gauss(x,A,x0,sigma):
    return A*np.exp(-(x-x0)**2/(2*sigma)**2)

def FitGaussian(imArray):
    #Extract middle line
    mid = round(imArray.shape[0]/2)
    midLine = imArray[mid]

    #Define x values, most likely peak position and most likely amplitude
    px = range(len(midLine))
    c = midLine.argmax(axis=0)
    A = midLine[c]

    popt,pcov = curve_fit(Gauss,px,midLine,p0=[A,c,len(midLine)/2])
    return popt

def ExtractSigma(filename):
    #Read image
    imArray = ReadImage(filename)

    #Extract fitting data
    popt = FitGaussian(imArray)

    #Return absolute value of Sigma
    return np.abs(popt[-1])

def SaveDataImage(filename, savename=None):
    if savename==None:
        savename = os.path.splitext(filename)[0] +'.png'

    imArray = ReadImage(filename)

    #Save image
    image = PIL.Image.fromarray(imArray)
    image.save(savename)
    print('Image saved at: ' + savename)


if __name__=='__main__':
    from tkinter.filedialog import askopenfilename
    #Select file
    filename = askopenfilename()
    SaveDataImage(filename)
